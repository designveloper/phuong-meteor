# [Building an App with React.js and MeteorJS](https://www.lynda.com/React-js-tutorials/Building-App-React-js-MeteorJS/533228-2.html)

## Chapter 01 - Setting up

### Setting up your editor with plugins
* Plugin for working with JSX: `language-babel`

### Initial project setup with meteor and NPM
* Create app: `meteor create <name>`
* Install package: `meteor npm install ...`
* Change version of meteor: edit `.meteor/release` file

## Chapter 03 - Reduce Player Stats

### Meteor introduction
* Meteor package: [atmospherejs.com/](https://atmospherejs.com/)

### Yarn the Facebook package manager
* `Yarn` is a package installer like `NPM`
* It's faster, but also has a lot around versioning dependencies and security going under the hood
* Install yarn: `npm install -g yarn`
* Then we only use `yarn` command to install all the dependencies
* To add a package: `yarn add <package-name>`, and it will install that package, as well as save the package to the dependencies (in `package.json` file)

### Creating a schema

```javascript
import { Mongo } from "meteor/mongo";

export const Players = new Mongo.Collection("players");

const PlayerSchema = new SimpleSchema({
  name: { type: String },
  team: { type: String },
  ...
});

Players.attachSchema(PlayerSchema);
```

### Subscribe and publish
* remove the `autopublish` package in `.meteor/packages` to make our application a bit more secure
* then we'll code our publications and subscriptions
* add a new package called `react-meteor-data` this package allow us to create a container which will set up the state and load the data from our server)

### Add a user login
* Install package:
    * `meteor add accounts-ui accounts-facebook`
    * `yarn add bcrypt`
* `meteor npm rebuild`

### Securing our DB transactions with server methods
* Remove `insecure` package from `.meteor/packages`
* By removing that, if we don't do anything else, we wouldn't be able to add data to database
