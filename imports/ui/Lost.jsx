import React from "react"

export default class Lost extends React.Component {
    render() {
        return (
            <h1>Not found - go back</h1>
        )
    }
};
